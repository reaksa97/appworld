export const navItems = [
    {
      id: 1,
      title: "Home",
      path: "./",
      cName: "nav-item",
    },
    {
      id: 2,
      title: "Services",
      path: "./services",
      cName: "nav-item",
    },
    {
      id: 3,
      title: "Products",
      path: "./products",
      cName: "nav-item",
    },
    {
      id: 4,
      title: "Contact Us",
      path: "./contactus",
      cName: "nav-item",
    },
  ];
  
  export const serviceDropdown = [
    {
      id: 1,
      title: "Clotheses",
      path: "./Clotheses",
      cName: "submenu-item",
    },
    {
      id: 2,
      title: "Shos",
      path: "./Shoes",
      cName: "submenu-item",
    },
    {
      id: 3,
      title: "Bag",
      path: "./Bag",
      cName: "submenu-item",
    },
    {
      id: 4,
      title: "Other",
      path: "./Other",
      cName: "submenu-item",
    },
  ];
  // export const Mendrop={
  //   id: 1,
  //   title: "ALL",
  //   path: "./ALL",
  //   cName: "submenu-item",
  // }