import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import "../styles/NavBar.css";
import { BiStore } from "react-icons/bi";
import Button from './Button'
import { navItems } from "../helper/ItemNavBar";
import Dropdown from './Dropdown';
import { AiOutlineMenu} from "react-icons/ai";

function NavBar() {
    const [dropdown, setDropdown] = useState(false);
    const [openLinks, setOpenLinks] = useState(false);

    const toggleNavbar = () => {
        setOpenLinks(!openLinks);
    };
    return (
        <>
            <nav className="navbar">
                <Link to="/" className='navbar-logo'>
                    CLOTHES
                    <br/>
                    <BiStore />
                </Link>
                <ul className='nav-items'>

                    {navItems.map((item) => {
                        if (item.title === "Products") {
                            return (
                                <li
                                    key={item.id}
                                    className={item.cName}
                                    onMouseEnter={() => setDropdown(true)}
                                    onMouseLeave={() => setDropdown(false)}

                                >
                                    <Link to={item.path}>{item.title}</Link>
                                    {dropdown && <Dropdown />}
                                </li>
                            );
                        }
                       
                        return (

                            <li key={item.id} className={item.cName} >
                                <Link to={item.path}>{item.title}</Link>

                            </li>

                        );
                    })}
                   

                    <Button />
                    


                </ul>
                {
                        <div className="rightSide">
                             <button className='Btn' onClick={toggleNavbar}>
                                <AiOutlineMenu/>
                            </button>
                            <Link className='m' to="/"> Home </Link>
                            <Link className='m' to="/services"> Services </Link>
                            <Link className='m' to="/products"> Product </Link>
                            <Link className='m'  to="/contact"> Contact us </Link>
                           
                        </div>
                    }

            </nav>
        </>
    )
}

export default NavBar;